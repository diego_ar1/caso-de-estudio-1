﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CasoUno
{
    public class Productos
    {
        public DateTime FechaCaducidad { get; set; }

        public string NumeroLote { get; set; }

        public DateTime FechaEnvasado { get; set; }

        public string PaisOrigen { get; set; }
    }
}
