﻿using System;

namespace CasoUno
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Producto Fresco
            ProductosFrescos productoFresco = new ProductosFrescos();
            productoFresco.FechaCaducidad = DateTime.Parse("07/05/2020");
            productoFresco.NumeroLote = "01";
            productoFresco.FechaEnvasado = DateTime.Now;
            productoFresco.PaisOrigen = "Mexico";

            //Productos Frios
            ProductosFrios productosFrios = new ProductosFrios();
            productosFrios.FechaCaducidad = DateTime.Parse("07/05/2020");
            productosFrios.NumeroLote = "01";
            productosFrios.FechaEnvasado = DateTime.Now;
            productosFrios.PaisOrigen = "Mexico";
            productosFrios.TempRecomendada = "15";

            //Productos Refrigerados
            ProductosRefrigerados productosRefrigerados = new ProductosRefrigerados();
            productosRefrigerados.FechaCaducidad = DateTime.Parse("07/05/2020");
            productosRefrigerados.NumeroLote = "01";
            productosRefrigerados.FechaEnvasado = DateTime.Now;
            productosRefrigerados.PaisOrigen = "Mexico";
            productosRefrigerados.TempRecomendada = "15";
            productosRefrigerados.CodigoSupervision = "COD_SUPERVISION";

            //Congelados por aire
            CongeladosAire congeladosAire = new CongeladosAire();
            congeladosAire.FechaCaducidad = DateTime.Parse("07/05/2020");
            congeladosAire.NumeroLote = "01";
            congeladosAire.FechaEnvasado = DateTime.Now;
            congeladosAire.PaisOrigen = "Mexico";
            congeladosAire.TempRecomendada = "15";
            congeladosAire.PorcenNitrogeno = "15";
            congeladosAire.PorcenOxigeno = "15";
            congeladosAire.PorcenDioxidoCarbono = "15";
            congeladosAire.PorcenVaporAgua = "15";

            //Congelados por Agua
            CongeladosAgua congeladosAgua = new CongeladosAgua();
            congeladosAgua.FechaCaducidad = DateTime.Parse("07/05/2020");
            congeladosAgua.NumeroLote = "01";
            congeladosAgua.FechaEnvasado = DateTime.Now;
            congeladosAgua.PaisOrigen = "Mexico";
            congeladosAgua.TempRecomendada = "15";
            congeladosAgua.Salinidad = "15";

            //Congelados por Nitrogeno
            CongeladosNitrogeno congeladosNitrogeno = new CongeladosNitrogeno();
            congeladosNitrogeno.FechaCaducidad = DateTime.Parse("07/05/2020");
            congeladosNitrogeno.NumeroLote = "01";
            congeladosNitrogeno.FechaEnvasado = DateTime.Now;
            congeladosNitrogeno.PaisOrigen = "Mexico";
            congeladosNitrogeno.TempRecomendada = "15";
            congeladosNitrogeno.MetodoCongelacion = "Metodo normal";
            congeladosNitrogeno.TiempoExposicion = "15";

            Console.WriteLine("El producto fresco su fecha de caducidad es: " + productoFresco.FechaCaducidad.ToShortDateString() + ", su numero de lote es: " + productoFresco.NumeroLote
                               + ", Su fecha de envasado es: " + productoFresco.FechaEnvasado.ToShortDateString() + " y su pais de origen es: " + productoFresco.PaisOrigen + "."
                               + "El producto frio su fecha de caducidad es: " + productosFrios.FechaCaducidad.ToShortDateString() + ", su numero de lote es: " + productosFrios.NumeroLote
                               + ", Su fecha de envasado es: " + productosFrios.FechaEnvasado.ToShortDateString() + " y su pais de origen es: " + productosFrios.PaisOrigen + " y su temperatura recomendad es: " + productosFrios.TempRecomendada + "."
                               + "El producto refrigerado su fecha de caducidad es: " + productosRefrigerados.FechaCaducidad.ToShortDateString() + ", su numero de lote es: " + productosRefrigerados.NumeroLote
                               + ", Su fecha de envasado es: " + productosRefrigerados.FechaEnvasado.ToShortDateString() + " y su pais de origen es: " + productosRefrigerados.PaisOrigen + " y su temperatura recomendad es: " + productosRefrigerados.TempRecomendada + " y su " +
                               "código de supervision es: " + productosRefrigerados.CodigoSupervision + "."
                               + "El producto congelado por aire su fecha de caducidad es: " + congeladosAire.FechaCaducidad.ToShortDateString() + ", su numero de lote es: " + congeladosAire.NumeroLote
                               + ", Su fecha de envasado es: " + congeladosAire.FechaEnvasado.ToShortDateString() + " y su pais de origen es: " + congeladosAire.PaisOrigen + " y su temperatura recomendad es: " + congeladosAire.TempRecomendada + " y " +
                               "su porcentaje de nitrogeno es: " + congeladosAire.PorcenNitrogeno + ", porcentaje de oxigeno es: " + congeladosAire.PorcenOxigeno + ", porcentaje de dioxido de carbono es:" + congeladosAire.PorcenDioxidoCarbono + ", porcentaje de vapor de agua es:" + congeladosAire.PorcenVaporAgua + "."
                               + "El producto congelado por agua su fecha de caducidad es: " + congeladosAgua.FechaCaducidad.ToShortDateString() + ", su numero de lote es: " + congeladosAgua.NumeroLote
                               + ", Su fecha de envasado es: " + congeladosAgua.FechaEnvasado.ToShortDateString() + " y su pais de origen es: " + congeladosAgua.PaisOrigen + " y su temperatura recomendad es: " + congeladosAgua.TempRecomendada + ", su salinidad es: " + congeladosAgua.Salinidad + "."
                               + "El producto congelado por nitrogeno fecha de caducidad es: " + congeladosNitrogeno.FechaCaducidad.ToShortDateString() + ", su numero de lote es: " + congeladosNitrogeno.NumeroLote
                               + ", Su fecha de envasado es: " + congeladosNitrogeno.FechaEnvasado.ToShortDateString() + " y su pais de origen es: " + congeladosNitrogeno.PaisOrigen + " y su temperatura recomendad es: " + congeladosNitrogeno.TempRecomendada + ", su metodo de congelacion es: " + congeladosNitrogeno.MetodoCongelacion
                               + ", su tiempo de exposicion al nitrogeno es: " + congeladosNitrogeno.TiempoExposicion + ".");
        }
    }
}
