﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CasoUno
{
    public class CongeladosAire : ProductosFrios
    {
        public string PorcenNitrogeno { get; set; }
        public string PorcenOxigeno { get; set; }
        public string PorcenDioxidoCarbono { get; set; }
        public string PorcenVaporAgua { get; set; }
    }
}
